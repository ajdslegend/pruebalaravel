<?php
/**
 * Created by PhpStorm.
 * User: mariagarcia
 * Date: 25/10/2018
 * Time: 5:30
 */

namespace App;

use App\User as u;

use \Firebase\JWT\JWT;

class AuthCustom
{
    public static function getUser($request)
    {
        try {
            $app_key = env('APP_KEY');
            $authorization = $request->header('authorization');
            $splitToken = explode(' ', $authorization);
            //$token="";
            if ($splitToken[0] == 'bearer' || $splitToken[0] == 'Bearer') {
                $token = $splitToken[1];
                $payload = JWT::decode($token, $app_key, array('HS256'));
                $usr = u::where('email', $payload->email)->first();
                return $usr;
            } else {
                return response()->json(["success" => false, "code" => 403, "message" => "requiere token"]);
            }
        } catch (\Exception $e) {
            return response()->json(["success" => false, "code" => 403, "message" => $e->getMessage()]);
        }
    }
}