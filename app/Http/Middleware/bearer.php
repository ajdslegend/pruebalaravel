<?php

namespace App\Http\Middleware;

use Closure;
use App\User as u;

use \Firebase\JWT\JWT;
#use Illuminate\Contracts\Routing\Middleware;
#use Illuminate\Http\Response;

class bearer
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $app_key = env('APP_KEY');
        $authorization = $request->header('authorization');
        $splitToken = explode(' ', $authorization);
        //$token="";
        if ($splitToken[0] == 'bearer' || $splitToken[0]=='Bearer') {
            $token = $splitToken[1];
            $payload = JWT::decode($token, $app_key, array('HS256'));
            $usr = u::where('email', $payload->email)->count();
            if ($usr == 0) {
                // abort(403, "Unauthorized. ! required token");
                return response()->json(["success" => false, "code" => 403, "message" => "required token"]);
            }
        } else {
            //abort(403, "Unauthorized. !!!");
            return response()->json(["success" => false, "code" => 403, "message" => "required token"]);
        }

        return $next($request);
    }
}