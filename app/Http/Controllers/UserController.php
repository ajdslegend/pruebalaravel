<?php

namespace App\Http\Controllers;

use App\Notifications\ResetPassword;
use Illuminate\Http\Request;

use App\User as u;

use Illuminate\Support\Facades\Hash;

use \Firebase\JWT\JWT;

use App\AuthCustom as auth;

// use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        try {
            $data = $request->all();
            // dd($data);
            //Log::info("get id: " . $data );

            $rules = array(
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6'
            );

            $v = \Validator::make($data, $rules);
            if ($v->fails()) {
                return response()->json(["success" => false, "message" => $v->errors()->all(), "data" => ""]);
            } else {
                $usr = u::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                ]);

                return response()->json(["success" => true, "message" => "ok", "data" => $usr]);
            }

        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $app_key = env('APP_KEY');
        try {
            $email = $request->input('email');
            $password = $request->input('password');
            $user = u::where('email', $email)->first();
            // $usr = "";
            if (Hash::check($password, $user->password)) {
                $payload = array(
                    'name' => $user->name,
                    'email' => $user->email
                );
                $usrs = JWT::encode($payload, $app_key);
                $usr = ["type" => "Bearer", "token" => $usrs];
                return response()->json(["success" => true, "message" => "ok", "data" => $usr]);
            } else {
                return response()->json(["success" => false, "message" => ["Contraseña invalida"], "data" => ""]);
            }
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => ["Usuario no encontrado"], "data" => ""]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function auth(Request $request)
    {
        try {
            $usr = auth::getUser($request);
            return response()->json(["success" => true, "message" => "ok", "data" => $usr]);
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage()]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        try {

            $usr = u::all();

            return response()->json(["success" => true, "message" => "ok", "data" => $usr]);


        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $data = $request->all();
            // dd($data);
            //Log::info("get id: " . $data );

            $rules = array(
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6'
            );

            $v = \Validator::make($data, $rules);
            if ($v->fails()) {
                return response()->json(["success" => false, "message" => $v->errors()->all(), "data" => ""]);
            } else {
                $usr = u::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                ]);

                return response()->json(["success" => true, "message" => "ok", "data" => $usr]);
            }

        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        try {

            $usr = u::where('id', $id)->first();

            return response()->json(["success" => true, "message" => "ok", "data" => $usr]);


        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            // dd($data);
            //Log::info("get id: " . $data );

            $rules = array(
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|min:6'
            );

            $v = \Validator::make($data, $rules);
            if ($v->fails()) {
                return response()->json(["success" => false, "message" => $v->errors()->all(), "data" => ""]);
            } else {
                $data['password'] = bcrypt($request->input('password'));
                $usr = u::where('id', $id)->update($data);

                return response()->json(["success" => true, "message" => "ok", "data" => $usr]);
            }

        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {

            $usr = u::where('id', $id)->delete();

            return response()->json(["success" => true, "message" => "ok", "data" => $usr]);

        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(Request $request)
    {
        try {
            // $app_key = env('APP_KEY');
            $data = $request->all();
            // dd($data);
            //Log::info("get id: " . $data );

            $rules = array(
                'email' => 'required'
            );

            $v = \Validator::make($data, $rules);
            if ($v->fails()) {
                return response()->json(["success" => false, "message" => $v->errors()->all(), "data" => ""]);
            } else {
                $count = u::where('email', $request->input('email'))->count();

                if ($count != 0) {
                    $user = u::where('email', $request->input('email'))->first();


                    $token = str_random(40);

                    $user->remember_token = $token;
                    $user->save();


                    $body = ['token' => $token];
                    $user->notify(new ResetPassword($body));

                    return response()->json(["success" => true, "message" => "Se ha enviado un email. Por favor verifique", "data" => null]);
                } else {
                    return response()->json(["success" => false, "message" => ["Email no corresponde a la cuenta"], "data" => null]);
                }
            }

        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(Request $request)
    {
        try {
            $data = $request->all();

            $rules = array(
                'password' => 'required'
            );

            $v = \Validator::make($data, $rules);
            if ($v->fails()) {
                return response()->json(["success" => false, "message" => $v->errors()->all(), "data" => ""]);
            } else {

                $user = u::where('remember_token', $request->input('token'))->first();
                $user->password = bcrypt($request->input('password'));
                $user->remember_token = null;
                $user->save();

                return response()->json(["success" => true, "message" => "Se ha recuperado la contraseña", "data" => null]);
            }

        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage(), "data" => ""]);
        }
    }
}
