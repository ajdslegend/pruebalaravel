<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['cors:api'])->post('/register','UserController@register');

Route::middleware(['cors:api'])->post('/login','UserController@login');

Route::middleware(['cors:api','bearer'])->get('/auth','UserController@auth');

Route::middleware(['cors:api','bearer'])->get('/user/all','UserController@all');

Route::middleware(['cors:api','bearer'])->post('/user/create','UserController@create');

Route::middleware(['cors:api','bearer'])->get('/user/edit/{id}','UserController@edit');

Route::middleware(['cors:api','bearer'])->put('/user/update/{id}','UserController@update');

Route::middleware(['cors:api','bearer'])->get('/user/delete/{id}','UserController@delete');

Route::middleware(['cors:api'])->post('/forgot-password','UserController@forgotPassword');

Route::middleware(['cors:api'])->post('/reset-password','UserController@resetPassword');