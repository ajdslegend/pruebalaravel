<?php

return [

    /*
    |--------------------------------------------------------------------------
    | target Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'label_email' => 'Correo Electrónico',
    'label_password'     => 'Clave',
    'label_maintain_session' =>'Mantener la sesión',
    'label_log_in' =>'Iniciar Sesión',
    'label_main' =>'Principal',
    'label_slide' =>'Slide',
    'label_welcome' =>'Bienvenidos',
    'label_enter' =>'Entrar',
    'label_log_out'=>'Cerrar Sesión',
    'label_profile'=>'Perfil',
    'label_change_password'=>'Cambiar Clave',
    'label_dashboard'=>'Panel',
    'label_create_product'=>'Crear Producto',
    'label_product'=>'Productos',
    'label_sale'=>'Ventas',
    'label_tax'=>'Impuesto',
    'label_detail_product'=>'Detalles de Productos',
    'label_category'=>'Categoria',
    'label_create_category'=>'Crear Categoria',
    'label_sub_category'=>'Subcategoria',
    'label_create_tax'=>'Crear Impuesto',
    'label_create_sub_category'=>'Crear Subcategoria',
    'label_method_payment'=>'Forma de Pago',
    'label_provider'=>'Proveedor',
    'label_create_provider'=>'Crear Proveedores',
    'label_manager_provider'=>'Administrar Proveedores',
    'label_manager_products'=>'Administrar Productos',
    'label_manager_tax'=>'Administrar Impuesto',
    'label_manager_product_brand'=>'Administrar Marcas de Productos',
    'label_product_brand'=>'Marca de Producto',
    'label_create_brand'=>'Crear Marca',
    'label_refund'=>'Devolución',
    'label_security'=>'Seguridad',
    'label_users'=>'Usuarios',
    'label_manager_users'=>'Administrar Usuarios',
    'label_AccessErrors'=>'No tiene acceso a este módulo',
    'label_Api'=>'Generador de Api',
    'label_Apis'=>'Api',
    'label_manager_api'=>'Administrar Api',
    'label_create_api'=>'Crear Api',
    'label_manager_payment'=>'Administrar Metodo de Pago',
    'label_payment'=>'Metodo de Pago',
    'label_create_payment'=>'Crear Metodo de Pago',
    'label_create_account_money'=>'Crear Moneda de Apertura',
    'label_locality'=>'Localidad',
    'label_address'=>'Dirección',
    'label_account-money'=>'Moneda de Apertura',
    'label_manager_account-money'=>'Administrar Moneda de Apertura',
    'label_virtual_cash'=>'Moneda Virtual',
    'label_create_locality'=>'Crear Localidad',
    'label_create_address'=>'Crear Dirección',
    'label_manager_locality'=>'Administrar Localidad',
    'label_manager_address'=>'Administrar Dirección',
    'label_manager_billing'=>'Administrar Factura',
    'label_billing'=>'Facturación',
    'label_detailBilling'=>'Detalles de la Orden',
    'label_create_billing'=>'Crear Factura',
    'label_order'=>'Ordenes de Compra',

];