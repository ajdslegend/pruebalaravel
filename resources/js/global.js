import Vue from 'vue'

const DOMAIN = "http://localhost:8000/api/";
var global = new Vue({
    data() {
        return {
            LOGIN: DOMAIN + 'login',
            REGISTER: DOMAIN + 'register',
            AUTH: DOMAIN + 'auth',
            CHANGE_PASSWORD: DOMAIN + 'reset-password',
            FORGOT_PASSWORD: DOMAIN + 'forgot-password',
            RESET_PASSWORD: DOMAIN + 'reset-password',
            LIST_USER: DOMAIN + 'user/all',
            CREATE_USER: DOMAIN + 'user/create',
            EDIT_USER: DOMAIN + 'user/edit/',
            UPDATE_USER: DOMAIN + 'user/update/',
            DELETE_USER: DOMAIN + 'user/delete/'
        }
    }
});

export default global