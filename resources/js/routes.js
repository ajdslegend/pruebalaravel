import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import DashboardComponent from './components/DashboardComponent'
import RegisterComponent from './components/RegisterComponent'
import LoginComponent from './components/LoginComponent'
import LogoutComponent from './components/LogoutComponent'
import UserComponent from './components/UserComponent'
import HomeComponent from './components/HomeComponent'

import ForgotPasswordComponent from './components/ForgotPasswordComponent'
import ResetPasswordComponent from './components/ResetPasswordComponent'

import store from './store'


const routes = [
    {
        path: '/',
        redirect: {name: '/main'}
    },
    {
        path: '/main',
        name: 'main',
        component: DashboardComponent,
        children: [
            {
                path: '/user',
                name: 'user',
                component: UserComponent
            },
            {
                path: '/home',
                name: 'home',
                component: HomeComponent
            }
        ],
        meta: {requiresAuth: true}
    },
    {
        path: '/forgot-password',
        name: 'forgot-password',
        component: ForgotPasswordComponent
    },
    {
        path: '/reset-password/:token',
        component: ResetPasswordComponent
    },
    {
        path: '/login',
        name: 'login',
        component: LoginComponent
    },
    {
        path: '/register',
        name: 'register',
        component: RegisterComponent
    },
    {
        path: '/logout',
        name: 'logout',
        component: LogoutComponent
    }
]

const router = new VueRouter({
    routes
})

//
router.beforeEach((to, from, next) => {

    // check if the route requires authentication and user is not logged in
    if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
        // redirect to login page
        next({path: '/login'})
        return
    }
    // if logged in redirect to dashboard
    if (to.path === '/login' && store.state.isLoggedIn) {
        next({path: '/home'})
        return
    }

    if (to.path === '/register' && store.state.isLoggedIn) {
        next({path: '/home'})
        return
    }

    next()
});


export default router

