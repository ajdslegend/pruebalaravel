<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{!! asset('bower_components/bootstrap/dist/css/bootstrap.css') !!}" rel="stylesheet">
    <link href="{!! asset('bower_components/bootstrap/dist/css/bootstrap-grid.css') !!}" rel="stylesheet">
    <link href="{!! asset('bower_components/bootstrap/dist/css/bootstrap-reboot.css') !!}" rel="stylesheet">
    <link href="{!! asset('bower_components/bootstrap-material-design/assets/css/docs.css') !!}" rel="stylesheet">

</head>
<body>
<div id="app">
    <app-component></app-component>
</div>
<script src="{!! asset('bower_components/vue/dist/vue.min.js') !!}"></script>
<script src="{!! asset('js/app.js') !!}"></script>

<script src="{!! asset('bower_components/jquery/dist/jquery.min.js') !!}"></script>
<script src="{!! asset('bower_components/jquery/dist/jquery.slim.min.js') !!}"></script>

<script src="{!! asset('bower_components/bootstrap/dist/js/bootstrap.js') !!}"></script>

<script src="{!! asset('bower_components/bootstrap-material-design/assets/js/docs.min.js') !!}"></script>
</body>
</html>